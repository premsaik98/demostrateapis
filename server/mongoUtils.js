const mongoose = require('mongoose');
const dbURL = "mongodb+srv://premsai:prem123@mycluster.ssenl.mongodb.net/bookStore"

function mongooseConnect(){
    mongoose.connect(dbURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(res =>{
        console.log("Connected to Database BookStore...")
    })
    .catch(err => console.log(err))
}


module.exports = { mongooseConnect }