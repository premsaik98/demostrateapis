const express = require("express")
const mongoUtils = require("./mongoUtils")
const { BookModel , AuthorModel } = require('./model/book.model')
const bodyParser = require("body-parser")
const cors = require("cors")
const app = express();

const port = process.env.PORT || 8080;

mongoUtils.mongooseConnect();
app.use(bodyParser.json());
app.use(cors());

 

app.get("/authors", (req, res) =>{
    AuthorModel.find()
    .then(data => {
        return res.send(data);
    })
    .catch(err => console.log(err))
})

app.post("/authors", (req, res) =>{
    if(req.body){
        console.log(req.body);
        const authorDetails = new AuthorModel(req.body);
        authorDetails.save()
        .then(data => {
            return res.send(data);
        })
        .catch(err => console.log(err))
    }
});


app.get("/books", (req, res) =>{
    BookModel.find()
    .then(data => {
        return res.send(data);
    })
    .catch(err => console.log(err))
})


app.post("/books", (req, res) =>{
    if(req.body){
        console.log(req.body);
        const bookDetails = new BookModel(req.body);
        bookDetails.save()
        .then(data => {
            return res.send(data);
        })
        .catch(err => console.log(err))
    }
});


app.get("/get-authors-by-book-name/:bookName", (req, res) => {
    if(req.params){
        const bookName = req.params.bookName;
        const query = { bookName : bookName}
        console.log(bookName)
        BookModel.findOne(query)
        .then(data => {
          //  console.log(data)
            return res.send(data.bookAuthor)
        })
        .catch(err => console.log(err))
    }
})

app.patch("/authors/:authorName" , (req , res) => {

    if(req.params){
        const authorName = req.params.authorName;
        const filter = { authorName : authorName};

        AuthorModel.findOneAndUpdate(filter, req.body , false)
        .then(data => {
            console.log("Successfully updated details for: "+authorName)
            return res.send(data);
        })
        .catch(err => console.log(err))
    }
})

app.delete("/books/:bookName", (req, res) =>{
    if(req.params){
        const bookName = req.params.bookName;
        const query = { bookName: bookName }

        BookModel.findOneAndDelete(query)
        .then(data => {
            return res.send(data);
        })
        .catch(err => console.log(err))
    }
})

app.listen(port,() => { 
    console.log("Listening at PORT: 8080");
});