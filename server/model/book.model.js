const mongoose = require('mongoose')

const authorSchema = mongoose.Schema({
    authorName:{
        type: String,
        required: true,
    },
    authorAge: {
        type: String,
        required: true,
    },
    authorGender: {
        type: String,
        required: true,
    },
    authorAwards:{
        type: String,
        required: true,
    }
})

const bookSchema = mongoose.Schema({
    isbn: 
    {
        type: String,
        required: true,
        unique: true
    },
    bookName: {
        type: String,
        required: true
    },
    bookAuthor: [String],
    bookPublisher:{
        type: String,
        required: true
    },
    bookRating:{
        type: Number,
        required: true
    },
    bookPrice: {
        type: Number,
        required: true,
    }

})

const BookModel = mongoose.model("books",bookSchema)
const AuthorModel = mongoose.model("authors",authorSchema)


module.exports = { BookModel, AuthorModel }

